**Требования:**

- PHP &gt;= 7.1.3
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Ctype PHP Extension
- JSON PHP Extension

**Инструкция по запуску**

1. Клонируем проект с Bitbucket – a

git clone [h](https://%5Busername%5D@bitbucket.org/Mukhammad911/demo_rest_api.git) [ttps://](https://%5Busername%5D@bitbucket.org/Mukhammad911/demo_rest_api.git) [_[username]_](https://%5Busername%5D@bitbucket.org/Mukhammad911/demo_rest_api.git) [@bitbucket.org/Mukhammad911/demo\_rest\_api.git](https://%5Busername%5D@bitbucket.org/Mukhammad911/demo_rest_api.git)

Открываем консоль в директории проекта и запускаем команду

composerinstall (Прежде этого мы должны установить composer)

1. Создаем БД и прописываем в .env данные для коннекта в БД (DB\_DATABASE, DB\_USERNAME, DB\_PASSWORD)
2. Запускаем команду phpartisanmigrate в консоли
3. Запускаем команду phpartisanpassport:keys --force в консоли
4. Запускаемпроект php artisan serve –port=[порт]

**Инструкция по применению**  **API**

В данном демо для взаимодействия с API используется OAuth2 для авторизации запросов.

Время истечения access\_token прописано 3 минуты в сервис провайдере AuthServiceProvider метод boot()

Ниже приведена таблица роутов.



| **HTTP verb** | **URI** | **Headers** | **Params** | **Response** | **Description** |
| --- | --- | --- | --- | --- | --- |
| POST | /api/register | Accept: application/json Content-Type: application/x-www-form-urlencoded | name – string(255) username – string(255) email password – минимум 6 символов redirect\_url | {    "client\_name": "Username",    "client\_id": 1,    "client\_user\_id": 1,    "client\_secret": "secret",    "client\_redirect\_url": "http://example.com"} | Регистрация пользователя и клиента для него для дальнейшей работы с OAuth2 |
| POST | /oauth/token | Accept: application/json Content-Type: application/x-www-form-urlencoded | grant\_type – (password или refresh\_token) client\_id – id клиента client\_secret – секретный ключ клиента scope – будем ставить \* username – имя пользователя (используется для grant\_typepassword) password – пароль пользователя (используется для grant\_typepassword) refresh\_token –(использовать если grant\_type передаем refresh\_token) | {    "token\_type": "Bearer",    "expires\_in": 180,    "access\_token": "…",    "refresh\_token": "…."} | Формирование access\_token и refresh\_token для использования в получение доступа к API |
| GET | /api/category | Accept: application/json Content-Type: application/x-www-form-urlencoded Authorization: Bearer (вводим access\_token) | - | {    "status": true,    "categories": [        {            "id": 1,            "name": "…",            "description": "….",            "created\_at": "..",            "updated\_at": ".."        },        {            "id": 2,            "name": "….",            "description": "…",            "created\_at": "..",            "updated\_at": "…"        }   ]} | Возвращает все категории |
| GET | /api/category/{ид\_категории} | Accept: application/json Content-Type: application/x-www-form-urlencoded Authorization: Bearer (вводим access\_token) | - | {"id": 1, "name": "…", "description": "….", "created\_at": "..",  "updated\_at": ".."} | Возвращает одну категорию по id |
| GET | /api/category/{ ид\_категории }/products | Accept: application/json Content-Type: application/x-www-form-urlencoded Authorization: Bearer (вводим access\_token) | - | {    "status": true,    "category\_id": 5,    "products": [        {            "id": 1,            "category\_id": 5,            "name": "...",            "price": "....",            "created\_at": "....",            "updated\_at": "...."        },        {            "id": 2,            "category\_id": 5,            "name": ".....",            "price": ".....",            "created\_at": "....",            "updated\_at": "....."        },        {            "id": 3,            "category\_id": 5,            "name": ".....",            "price": ".....",            "created\_at": "....",            "updated\_at": "....."        }   ]} | Возвращает продукты  по категории |
| POST | /api/category | Accept: application/json Content-Type: application/x-www-form-urlencoded Authorization: Bearer (вводим access\_token) | name – имя категории description – описание категории  |   | Создает категорию |
| POST (PUT) | /api/category/{ид\_категории} | Accept: application/json Content-Type: application/x-www-form-urlencoded Authorization: Bearer (вводим access\_token) | name – имя категории  description – описание категории \_method – значение ставим тут PUT |   | Обновляет категорию |
| POST (DELETE) | /api/category/{ид\_категории} | Accept: application/json Content-Type: application/x-www-form-urlencoded Authorization: Bearer (вводим access\_token) | \_method – значение ставим тут DELETE |   | Удаляет категорию |
| GET | /api/product | Accept: application/json Content-Type: application/x-www-form-urlencoded Authorization: Bearer (вводим access\_token) | - |   | Возвращает все продукты |
| GET | /api/product/{ид\_продукта} | Accept: application/json Content-Type: application/x-www-form-urlencoded Authorization: Bearer (вводим access\_token) | - |   | Возвращает один продукт по id |
| POST | /api/product | Accept: application/json Content-Type: application/x-www-form-urlencoded Authorization: Bearer (вводим access\_token) | category\_id – id категории в который входит продукт name – имя продукта price – цена продкута  |   | Создает продукт |
| POST (PUT) | /api/product/{ид\_продукта} | Accept: application/json Content-Type: application/x-www-form-urlencoded Authorization: Bearer (вводим access\_token) | category\_id – id категории в который входит продукт name – имя продукта price – цена продкута \_method – значение ставим тут PUT |   | Обновляет продукт |
| POST (DELETE) | /api/product/{ид\_продукта} | Accept: application/json Content-Type: application/x-www-form-urlencoded Authorization: Bearer (вводим access\_token) | \_method – значение ставим тут DELETE |   | Удаляет продукт |