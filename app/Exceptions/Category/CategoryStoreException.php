<?php

namespace App\Exceptions\Category;

use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

class CategoryStoreException extends Exception
{
    /**
     * Report or log an exception.
     *
     * @return void
     */
    public function report()
    {
        Log::debug('CategoryStoreException report worked!');
    }

    public function render()
    {
        return Response::json([
            'success' => false,
            'message' => 'Unhandled Error on storing Category to Database'
        ], 500);
    }
}
