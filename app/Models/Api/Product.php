<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function products()
    {
        return $this->belongsTo('App\Models\Api\Category');
    }
}
